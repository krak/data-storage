<?php

namespace Krak\DataStorage;

use Aws\S3\S3Client;

class S3DataStorage implements DataStorage
{
    private $client;
    private $bucket;
    private $prefix;

    public function __construct(S3Client $client, $bucket, $prefix = '')
    {
        $this->client = $client;
        $this->bucket = $bucket;
        $this->prefix = $prefix;
    }

    public function storeData($data, $mimetype, $path)
    {
        $result = $this->client->putObject([
            'Bucket' => $this->bucket,
            'Key' => $this->prefix . $path,
            'Body' => $data,
            'ContentType' => $mimetype,
        ]);

        return $result['ObjectURL'];
    }
}
