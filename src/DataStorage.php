<?php

namespace Krak\DataStorage;

interface DataStorage
{
    /**
     * @param string $data
     * @param string $mimetype
     * @param string $path
     */
    public function storeData($data, $mimetype, $path);
}
