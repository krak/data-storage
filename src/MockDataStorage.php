<?php

namespace Krak\DataStorage;

class MockDataStorage implements DataStorage
{
    public function storeData($data, $mimetype, $path) {
        return $path;
    }
}
